# esdump

Copy all indices from a source elasticsearch instance to a destination instance. This could take a while depending on how much data is present. You can, optionally, supply a list of indices to limit the scope of the copy.

USAGE: `./esdump -s 10.2.3.5 -d localhost [ -i index1,index2,index3 ]`
